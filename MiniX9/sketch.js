let weather;

function setup() {
	createCanvas(400, 200);
	loadJSON('https://api.openweathermap.org/data/2.5/weather?q=London&appid=064d467454a9a742a79d94809e9a7801&units=metric', gotData);
}

function gotData(data) {
	// println(data);
	weather = data;
}

function draw() {
	background(0);
	if(weather) {
		fill(200, 140, 23);
		ellipse(115, 100, weather.main.temp, weather.main.temp);
		fill(20, 111, 210);
		ellipse(270, 100, weather.main.humidity, weather.main.humidity);
	}
}
