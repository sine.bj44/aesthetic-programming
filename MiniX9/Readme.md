## MiniX9

[_Click to view our **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX9/)

[_Click to view our **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX9/sketch.js)

### What is the program about? Which API have you used and why?
The sketch that we ended up linking to for this minix, is a low floor program from a Daniel Shiffman video that draws two ellipses on the screen. Two of the parameters (width and height) get their values from “temperature” and “pressure”, which are properties in a dataset  about the weather in London, that we accessed through the API offered by openweathersource.com. To access the data we made an account and received our API keys, in a query url we then added “&” to the urls to prepare it for the next = “ADDID” followed by our key-numbers. In the tutorial, Shiffman suggests that you install a chrome add-on called: “JSON formatter”, which is an extension that structures the data in a way that makes it easier to read the different properties. 

![](myProject.png) *Our project*

### Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data? How much do you understand this data or what do you want to know more about? How do platform providers sort the data and give you the requested data? What are the power relations in the chosen APIs? What is the significance of APIs in digital culture?
We started out following Daniel Shiffmans video, where he teaches you how to use a weather API. We did this because we wanted to have a better understanding of how APIs work. After that we started working on a project where we were going to get an image of a bug through google, that we could display on our canvas. Our idea for this was to have it say “bug” in the code (when we choose what image to get) and then we would “de-bug” this code visually in our project by using a 10-print-like code that would cover the bug. We were going to do it using multiple white squares (as the background) and transparent squares, so we would cover some of the bug - but not all to symbolise how even if you remove a bug it is still there in the sense that you’ve learnt something from encountering it.

![](myFailedProject.PNG) *Our first project (that didn't work out)*

But we couldn’t make the image show up underneath the canvas, so the image would never be covered by the 10-print-like code. We still wanted to mention this experience though as we’ve learnt a lot about the limits you have using APIs sometimes. We could only use 100 images a day which resulted in us not being able to work further on our code until the next day, which was slightly irritating. We also had a conversation about who decides what bug-images we were getting by using this API, as we do not really understand it.

We ended up just using the Shiffman code as it also shows how the data we get through the weather website changes over time. With this API we had to choose what city and also what unit of measurement we wanted to use. Then we had to find the data we wanted through the url we got. 


### Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.
If we had more time we would like to work with two different questions:
- How do the APIs on social media such as reddit and twitter work?
- How do dominant API providers organise the information that they get from us? how are we being generalised? 

**References**
- Soon Winnie & Cox, Geoff, "Que(e)ry data", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 186-210
- Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019)
- 10.5: Working with APIs in Javascript - p5.js Tutorial: https://www.youtube.com/watch?v=ecT42O6I_WI&t=
