// Declare global variables.
  // If these were inside a function you could only call upon them in that specific function not outside of it (local variables).
let q1, q2, q3, q4;
let age, dating, single, gender, kids, noKids;
let backgroundIMG, beeper, newProfile, privacyPolicies, title, information;

// Preload the images to prevent lag.
function preload() {
  q1 = loadImage('images/genderQ.png');
  q2 = loadImage('images/relationshipQ.png');
  q3 = loadImage('images/kidsQ.png');
  q4 = loadImage('images/ageGroupQ.png');

  age = loadImage('images/age.png');
  dating = loadImage('images/dating.png');
  single = loadImage('images/single.png');
  gender = loadImage('images/gender.png');
  kids = loadImage('images/kids.png');
  noKids = loadImage('images/noKids.png');

  beeper = createImg('images/beep.gif');
  backgroundIMG = loadImage('images/BG.png');
  privacyPolicies = loadImage('images/privacyPolicies.png');
  newProfile = loadImage('images/newProfile.png');
  title = loadImage('images/title.png');
  information = loadImage('images/information.png');
}

// Set the framerate - the lower it is the slower it is.
function setup() {
  createCanvas(1200, 800);
  background(backgroundIMG);

// Gender answer-checkboxes.
  questionOne1 = createCheckbox('Female', false);
  questionOne1.changed(q1_checkbox1);
  questionOne1.position(119, 268);
  questionOne1.size(200, 200);

  questionOne2 = createCheckbox('Male', false);
  questionOne2.changed(q1_checkbox2);
  questionOne2.position(204, 268);
  questionOne2.size(200, 200);

  questionOne3 = createCheckbox('Other', false);
  questionOne3.changed(q1_checkbox3);
  questionOne3.position(284, 268);
  questionOne3.size(200, 200);


// Relationship answer-checkboxes.
  questionTwo1= createCheckbox('Yes', false);
  questionTwo1.changed(q2_checkbox1);
  questionTwo1.position(119, 368);
  questionTwo1.size(200, 200);

  questionTwo2 = createCheckbox('No', false);
  questionTwo2.changed(q2_checkbox2);
  questionTwo2.position(204, 368);
  questionTwo2.size(200, 200);


// Kids answer-checkboxes.
  questionThree1= createCheckbox('Yes', false);
  questionThree1.changed(q3_checkbox1);
  questionThree1.position(119, 468);
  questionThree1.size(200, 200);

  questionThree2 = createCheckbox('No', false);
  questionThree2.changed(q3_checkbox2);
  questionThree2.position(204, 468);
  questionThree2.size(200, 200);


// Age group answer-checkboxes.
  questionFour1= createCheckbox('Child', false);
  questionFour1.changed(q4_checkbox1);
  questionFour1.position(119, 568);
  questionFour1.size(200, 200);

  questionFour2 = createCheckbox('Teen', false);
  questionFour2.changed(q4_checkbox2);
  questionFour2.position(204, 568);
  questionFour2.size(200, 200);

  questionFour3 = createCheckbox('Adult', false);
  questionFour3.changed(q4_checkbox3);
  questionFour3.position(284, 568);
  questionFour3.size(200, 200);

  questionFour4 = createCheckbox('Elder', false);
  questionFour4.changed(q4_checkbox4);
  questionFour4.position(368, 568);
  questionFour4.size(200, 200);

// Creation of the button.
  button = createButton('Agree');
  button.position(250, 750);
  button.mouseClicked(agree);
}

// Center the images in the middle of the screen, draw the blueAnimation function and draw the throbber.
function draw() {
// Question images.
  image(q1, 116, 220);
  image(q2, 116, 320);
  image(q3, 115, 420);
  image(q4, 119, 520);

// Other images.
  image(privacyPolicies, 122, 680);
  image(newProfile, 800, 70);
  image(title, 95, 13);

// The gif.
  beeper.position(815, 350);
}

// What happens when you click a checkbox?
  // Question 1 results.
function q1_checkbox1() {
  if (this.checked()) {
    image(gender, 800, 115);
  } else {
    background(backgroundIMG);
  }
}

function q1_checkbox2() {
  if (this.checked()) {
    image(gender, 800, 115);
  } else {
    background(backgroundIMG);
  }
}

function q1_checkbox3() {
  if (this.checked()) {
    image(gender, 800, 115);
  } else {
    background(backgroundIMG);
  }
}

  // Question 2 results.
function q2_checkbox1() {
  if (this.checked()) {
    image(dating, 800, 165);
  } else {
    background(backgroundIMG);
  }
}

function q2_checkbox2() {
  if (this.checked()) {
    image(single, 800, 165);
  } else {
    background(backgroundIMG);
  }
}


  // Question 3 results.
function q3_checkbox1() {
  if (this.checked()) {
    image(kids, 800, 215);
  } else {
    background(backgroundIMG);
  }
}

function q3_checkbox2() {
  if (this.checked()) {
    image(noKids, 800, 215);
  } else {
    background(backgroundIMG);
  }
}


  // Question 4 results.
function q4_checkbox1() {
  if (this.checked()) {
    image(age, 800, 265);
  } else {
    background(backgroundIMG);
  }
}

function q4_checkbox2() {
  if (this.checked()) {
    image(age, 800, 265);
  } else {
    background(backgroundIMG);
  }
}

function q4_checkbox3() {
  if (this.checked()) {
    image(age, 800, 265);
  } else {
    background(backgroundIMG);
  }
}

function q4_checkbox4() {
  if (this.checked()) {
    image(age, 800, 265);
  } else {
    background(backgroundIMG);
  }
}

// Privacy policies agreement button.
function agree() {
  image(information, 735, 680);
}
