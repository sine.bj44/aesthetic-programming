## MiniX4

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX4/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX4/sketch.js)

### Short description of your work: **THEY**
My project is supposed to be a way to show how when you make a profile on a social media platform you're giving them a bunch of information that they sell off to third-parties. It can be sort of difficult to understand who "they" are, so I portrayed it as a little computer-guy, who's creating a profile based on your information. 

![](myProject.png) *My project*

### Describe your program and what you have used and learnt
My project this week shows you two sides. On the one side you can answer some questions and on the other side there's a little guy typing on a pc. When you select an answer you can then see what the guy is writing down on his pc. If you agree to the privacy policies by pressing the "agree" button then a text will show up on the other side saying "Information ready to be sold". Unlike when you press the checkbox the text that shows up from pressing the button doesn't go away if you press the button again. I did this to imitate how when you share information on a social media platform you can't _unshare_ it again. I let the final questions outcome be "We already knew this" to sort of show how they sometimes already have data on you. 

I learnt a lot about how important organising my code is this week. It's the longest code I've made so far and I realised it's important to have a good overview over what you want in your code. I used the `createCheckbox` for the first time, but it was luckily quite similar to creating a button, which I'd already tried in a different miniX. I didn't have much time this week, so I was quite happy with that part not being too difficult. I also _finally_ made gifs work! I tried doing that the first week, but I couldn't figure it out.

### What are the cultural implications of data capture?
I honestly can't name all the implications of data capture. In fact, I found it hard to come up with questions for my project because I realised I actually have no idea of just _how much_ data they're actually getting from each user. In short, it undermines the basic human right to privacy. 

All this data capture means that these companies know basically everything about you and they can use it however they want to. There are of course some regulation, but right now the tech industry is like the wild west. With the amount of money they can earn off of us users we need a signifigant amount of rules to regulate how they use our data.

**References**
- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary”
- Mejias, Ulises A. and Nick Couldry. "Datafication". Internet Policy Review 8.4 (2019). Web. 16 Feb. 2021
