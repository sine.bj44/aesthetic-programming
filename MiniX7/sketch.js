// Where is the sprite on the canvas when you first open the game.
let x = 125;
let y = 150;
let doorObjects, textObjects, imgObject, room, roomNight, door, door1, door2, desk, bed, screenCovid, player, rack, textBubble1, textBubble2, bedSheet, tallPlant, tallPlantHalf, smallPlant;

// Pre-render media (prevents lag before image appears).
function preload() {
    room = loadImage('images/Room.png');
    roomNight = loadImage('images/RoomNight.png');
    ghost = loadImage('images/Ghost.png');
    door = loadImage('images/Door.png');
    door1 = loadImage('images/Door1.png');
    door2 = loadImage('images/Door2.png');
    desk = loadImage('images/Desk.png');
    bed = loadImage('images/Bed.png');
    screenCovid = loadImage('images/Screen2.png');
    rack = loadImage('images/Rack.png');
    textBubble1 = loadImage('images/TextBubble1.png');
    textBubble2 = loadImage('images/TextBubble2.png');
    bedSheet = loadImage('images/BedSheet.png');
    tallPlant = loadImage('images/TallPlant.png');
    tallPlantHalf = loadImage('images/TallPlantHalf.png');
    smallPlant = loadImage('images/SmallPlant.png');
}

function setup() {
    createCanvas(250, 250);
    doorObjects = [new Door(168, 20, 46, 74, door, 9, 16.5), new Door(90, 198.5, 46, 74, door1, 9, 16.5), new Door(3.9, 140, 38, 74, door2, 9, 16.5)];
    textObjects = [new TextObject(214, 35, 20, 78, rack, 9, 16.5, 'You do not need a coat.'), new TextObject(96.5, 71, 30, 58, smallPlant, 9, 16.5, 'You admire the plant.'), new TextObject(6, 202, 55, 42, tallPlantHalf, 9, 16.5, 'This plant needs water.')];
    imgObject = [new ImgObject(186, 145.5, 50, 90, bed, 70, 30, roomNight), new ImgObject(28, 58, 40, 65, desk, 70, 30, screenCovid)];
}


function draw() {
    background(room);
    doorObjects[0].show();
    doorObjects[1].show();
    doorObjects[2].show();

    textObjects[0].show();
    textObjects[1].show();
    textObjects[2].show();

    imgObject[0].show();
    imgObject[1].show();

    // Making the sprite move.
      if (keyIsDown(LEFT_ARROW)) {
          x -= 2.5;
      }

      if (keyIsDown(RIGHT_ARROW)) {
          x += 2.5;
      }

      if (keyIsDown(UP_ARROW)) {
          y -= 2.5;
      }

      if (keyIsDown(DOWN_ARROW)) {
          y += 2.5;
      }

    if (keyCode === 32) {
        doorObjects[0].checkInteract(x, y);
        doorObjects[1].checkInteract(x, y);
        doorObjects[2].checkInteract(x, y);

        textObjects[0].checkInteract(x, y);
        textObjects[1].checkInteract(x, y);
        textObjects[2].checkInteract(x, y);

        imgObject[1].checkInteract(x, y);
    }

    // The sprite.
    push();
    imageMode(CENTER);
    player = image(ghost, x, y);
    pop();

    // These are here so you can be under the bedsheet or behind the plant.
    image(bedSheet, 186, 171);
    image(tallPlant, 5, 181.5);

    if (keyCode === 32) {
      imgObject[0].checkInteract(x, y);
    }
  }
