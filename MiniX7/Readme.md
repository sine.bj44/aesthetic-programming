## MiniX7

I had so much fun making this and I really hope you'll enjoy 'playing' it! _(It's supposed to be tiny just like games used to be on old handheld consoles, but if you can't see it well enough just zoom in)_

The keys you need to use to play are: `Arrow keys` and `spacebar`

[_Click to try my **game**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX7/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX7/sketch.js), [_**object 1**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX7/Door.js), [_**object 2**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX7/TextObject.js) and [_**object 3**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX7/ImgObject.js)

### How does/do your game/game objects work?
My game is about what it feels like for some people to be inside all the time during the pandemic. I personally haven't been hit too hard by the negative feelings from being inside, but some of my friends have and their feelings/experiences are what have inspired me to create this game. In my game you play as a ghost that can interact with many objects in the room. I wanted to give the player a feeling of "Oh that's all you can do ... wait, that's all _we_ can do during the pandemic" once the game has been completed (so when you've interacted with everything). 
Hopefully once the player has interacted with everything they'll try interacting with something again, but this time (or the 10th time - it doesn't matter) the player won't interact with _everything_. This I, as the creator, see as the beginning of lockdown vs now. In the beginning you found things to do in your apartment and now.. you've already 'explored' what there is to explore. So my game is not about winning or losing it's just there to show how lonely, bored, depressed etc. some people feel from being inside all the time. 


![](myProject.png) *My game*

### How you programmed the objects and their related attributes, and the methods in your game.
I've used three different objects in my program. Two of them are quite similar, and I definitely could've changed it so I would've only used one `class` instead of two, but I personally liked having the doors be by themselves and the other objects by themselves. They all have a common method that says whenever the sprite is within their radius (I guess you could put it like that) do x thing. Two of them display a speech-bubble (two different sized bubbles - this is where I could've merged them but didn't) at the top with text in them and the other has an image show up. I learned a lot about object oriented programming while making my game. I kept realising how I could add more attributes to the `class` in the `constructor` by using `this.`x/y/potato(whatever really) and then just defining what these things are when I create a `new` object. They also all have a `function show` that tells the class what this object looks like (it tells the program what to show).

### Draw upon the assigned reading, are there characteristics of object-oriented programming and the wider implications of abstraction?
I thought a lot about the text _The Obscure Objects of Object Orientation_ by Matthew Fuller and Andrew Goffey while I was creating the different objects in my game. I sat and decided what everything had to look like and especially as pixel art you leave a lot of details out. It was very apparent while making the objects using pixel art that they were abstracted versions of what the actual object looks like in real life. Sometimes it was quite difficult figuring out how to make certain things, because I didn't know if it was only me that thought it fx looked like slippers. Therefore I often asked my friends if they could guess what x object was. All the objects in my game would also normally have way more things you can do with them, but I obviously couldn't add all those options. But it also gave me the possibility to show something with my choice of method in a class. For an example the doors _look_ like doors, but they don't have the function of an _actual_ door. This is made to show how when you're inside your room all day you still have doors, but you can't use them because you can't leave due to the pandemic. 

I also decided to make the sprite be a ghost because then I didn't have to choose to make either a female or a male. By doing this I didn't have to merge all the many possibilites of what a human can look like into _one_ character. Yes, I did decide what my ghost should look like, but I felt like it came with less complications than making a human would. I also think that maybe you could see the ghost as how we might turn into a ghost of ourselves being alone for so long?

> _"... a computational object is a partial object: the properties and methods that define it are a necessarily selective and creative abstraction of particular capacities from the thing it models, and which specify the ways that it can be interacted with."_ - The Obscure Objects of Object Orientation by Matthew Fuller and Andrew Goffey, p. 6

### Your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?
An example of complex details and operations being abstracted could be seen in the game series The Sims. It's very clear to see that there are different classes (adults, teenager, elder, cat, dog), that you can choose between. You can choose their attributes and they've gotten more and more inclusive over the years, but when you are trying to create a sort of life-simulator you're bound to leave out complex details. The humans you're playing as don't in any way come close to what an actual human is like. A big factor would be that there are no disabled people in their games. This is a big indicator of how they're using the same class for fx all the adults and that in their abstraction of an adult they all behave/move around in the same way. 

> _"Object abstraction in computing is about representation. Certain attributes and relations are
> abstracted from the real world, whilst simultaneously leaving details and contexts out."_ - Aesthetic Programming (Object abstraction) by Winnie Soon and Geoff Cox, p. 146

**References**
- Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).

- I was also inspired by games like Stardew Valley or old Pokémon games, as they use pixelart, but I didn't look at the games as I was creating mine.
