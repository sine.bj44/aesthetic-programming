class Door {
    // The class Door is constructed of (X, Y, W, H, Img, T1, T2).
    constructor(X, Y, W, H, Img, T1, T2) {
        // Define what (X, Y) is. Fx you attach the property of 'img' to this object ➜ this.img = Img.
        this.x = X;
        this.y = Y;
        this.width = W;
        this.height = H;
        this.img = Img;
        this.text1 = T1;
        this.text2 = T2;
    }

    // If the players X is bigger than this.x and the players X is smaller than this.x + this.width. -||- for players Y. This means then the sprite is in the radius of the object do x.
    checkInteract(playerX, playerY) {
        if (playerX > this.x && playerX < this.x + this.width) {
            if (playerY > this.y && playerY < this.y + this.height) {
              image(textBubble1, 0, 0);
              fill('#131515');
              textSize(11);
              textFont('socket');
              text('This is not an exit.', this.text1, this.text2);
            }
        }
    }

    // What the object looks like.
    show() {
        image(this.img, this.x, this.y);
    }
}
