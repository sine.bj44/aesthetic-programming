// Declare global variables.
  // If these were inside a function you could only call upon them in that specific function not outside of it (local variables).
let fod1, fod2;
let counter = 0;
let imageState = 1;

// Preload the images to prevent lag.
function preload() {
  fod1 = loadImage('fod1.png');
  fod2 = loadImage('fod2.png');
}

// Set the framerate - the lower it is the slower it is.
function setup() {
  createCanvas(windowWidth, windowHeight);
  frameRate(50);
  background(150, 80);
}

// Center the images in the middle of the screen, draw the blueAnimation function and draw the throbber.
function draw() {
  imageMode(CENTER);
  blueAnimation();
  // counter + 1
  counter++;
  if(counter > 10) {
    // if the imageState is 1, then it becomes 2 - otherwise just be imageState 1. This makes it loop between the two stages.
    if(imageState == 1){
      imageState = 2;
    } else {
      imageState = 1;
    }
    counter = 0;
  }
  drawElements();
}

// The part of the code that declares what reaching each imageState means. If imageState is 2 show X image - if imageState is 1 show Y image.
function blueAnimation() {
  if(imageState == 2) {
    image(fod2, width/2, height/2);
  }
  if(imageState == 1) {
    image(fod1, width/2, height/2);
  }
}

// The throbber part of the code.
function drawElements() {
  let num = 500;
  stroke(240, 80);
  strokeWeight(11);
  noFill();
  ellipse(width/2, height/2, 422, 422);
  // Use push and pop to only make what's inside this area rotate.
push();
  // Place it in the middle of the screen.
  translate(width/2, height/2);
  let cir = 360/num*(frameCount%num);
  rotate(radians(cir));
  stroke('#7bbcc4');
  strokeWeight(10);
  point(211, 0);
  stroke('#f4c633');
  strokeWeight(10);
  point(211, 5);
  stroke('#d27a81');
  strokeWeight(10);
  point(211, 10);
pop();
}
