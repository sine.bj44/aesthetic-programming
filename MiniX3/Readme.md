## MiniX3

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX3/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX3/sketch.js)

### Describe your throbber design, both conceptually and technically.
First of all in this week some personal stuff have affected how I'll be doing this weeks minix, so I didn't have enough time to make what I really wanted to make.

I wanted to create a throbber that really made you feel like time was passing. So I started off with thinking about how in technology we often use real life things and implement them into software - just how we use folders or that it's called your desktop. I tried to think of when I feel like I'm _really_ waiting in real life. I ended up thinking about how waiting in line in airports or in themeparks feels like. I wanted my throbber to make you feel as if you're actually waiting in line, so therefore I drew people waiting in line with one of them waiting very impatiently (he's tapping his foot). I also made one of them be asleep and the other is looking at his clock to sort of give the viewer the idea of that these characters have already been waiting for a long time. The throbber around it is going pretty slowly to also give you the idea of how this will take a while to load. The colours of the people were also meant to be sort of dull to imply that these characters aren't happy. I also made the colours of the points in the throbber the same colour as the characters heads to sort of show that it's these three who are waiting in line. That way I'm sort of bringing both the throbber and the characters together in both the feeling of time and in the colours that were chosen. 

In this week's miniX I made the three points rotate around to sort of give the feeling of unending loading. The guy tapping his foot is also an infinite loop. I wanted to take the idea of the inifite loops into a real life feeling of when it feels like you're waiting forever. 

![](myProject.png) *My project*

### Consider what a throbber communicates, and/or hides?
To me a throbber tells me that I need to wait or that I've set an action in motion. As we've been told in class, throbbers also sometimes only serve that function - to let you know that you've succesfully set something in action. I do feel as if we have some sort of expectation that things are supposed to go fast when it's something online. Our patience is significally lower online than in real life - possibly because in real life you'd be making a minor scene by exiting a queue and online you just simply press **x** in the top corner. I was sort of hoping that my throbber would give you the instant feeling of "No, I don't want to wait for this" because you're instantly getting the feeling of "this is going to take a while". I wanted to kind of show the viewer how much the visuals of what you're seeing means. If the throbber is just mindlessly running you have no idea of how long this is going to take. My throbber doesn't show you how long you're going to wait either, but it does sort of imply that this isn't going to be fun or that it might take a while - would this affect how long you'd be willing to wait? 


**References**
- Soon Winnie & Cox, Geoff, "Infinite Loops", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 71-96
- Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation, 88-98, (2018)
