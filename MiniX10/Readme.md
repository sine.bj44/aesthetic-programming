## MiniX10

### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?
Generally speaking we did not have much trouble making the flowchart for our ideas, but we spent more time on trying to figure out and formulate our ideas, in order to have a specified starting point. Once we had most of it figured out, making the flowcharts went pretty fast.

The difficult part was to make something that would make sense for each member in the group in terms of the conceptualization of our ideas. We have included the essential overall features in our charts, but there are of course a lot of variables that have not been accounted for, that each member has a vague idea about. These are mostly the aesthetic values of our project ideas, whose symbolic meanings have yet to be determined once we choose an idea and dig deeper into its conceptual significance.

It should also be mentioned that we made our flowcharts on a Miro board, which enabled us all to make and edit them at the same time. This made it a lot easier in terms of communication and worked as a very good solution to making all of this online.

We observed that when the flowchart was made in the group prior to the code for our next project, the focus centered on defining the logical processes more than how the code would “think” and work. Thereby we had more focus on simple communication of the idea. This was opposed to some of our individual flowcharts, which due to the fact that we had a code prewritten, more easily had a focus on the algorithmic complexity, and the use of language in the flow charts also resembled how language was used on a code level.

### What are the technical challenges facing the two ideas and how are you going to address these?
We tried to bridge the gap in our skill levels by discussing each idea thoroughly, not just conceptually but also technically. Both ideas that were chosen have a low fidelity solution connected to it, ensuring that a final product can be achieved without members getting left out of the programming process. This is very important, as we want everyone to be able to talk about and explain the technical aspects of the code that we are going to make, as we all have our strengths and weaknesses in this course.

Personally, I felt like people had many different ideas as to how the project should be, and it was a bit difficult to figure out what to choose, which makes sense considering we're five people, but it all worked out nicely!

### In which ways are the individual and the group flowcharts you produced useful?
Personally, I found it a bit difficult to figure out how to do the individual flowchart as I haven't done many of them before and I've always felt as if they could easily be confusing, but after playing around with it for a bit it started to make more sense. It was definitely easier making the individual flowchart because it was made _after_ the code was already made whereas the group flowchart had to be made _before_ we'd made any code at all. 

The group flowcharts act as blueprints for the final sketch. Through dialogue and knowledge from the course all other details can be abstracted by each respective member.

![](Flowchart.png)
![](flowchart_dataficering.png)
![](flowchart_generative.png)

**References**
- Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226
- Taina Bucher, “The Multiplicity of Algorithims,” If...Then: Algorithmic Power andPolitics (Oxford: Oxford University Press, 2018)
- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013
