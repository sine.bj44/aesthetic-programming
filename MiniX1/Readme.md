## MiniX1

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX1/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX1/sketch.js)

### What have you produced?
I realised while staying at my parents house for Christmas, that my mum asks me multiple times a day "what does your pain feel like right now?". I often don't really know how to explain what chronic pain feels like - which is why I wanted to create something for my first MiniX1, that could help me with that.

I therefore decided to section the pain into three different levels: _bad_, _basic_ and _good_. But since those three words only explain the phsycial state of the pain and not the feelings that come with it too, I decided I wanted to visualise it through images. I came up with the idea to use an egg, because to me it also seems to have three different states: _yolk_, _egg_ and _chick_. I used these three states to visualise the three different levels of pain (it's obviously not as simple as that, but for this assignment I decided to break it into three levels). I drew the images and then got started with programming my idea. I started off with declaring my variables using `let`, whereafter I used the function `preload()` to preload my images. This function makes it so that `setup()` will wait until the images have finished loading. For the buttons I started out using `createButton()`, but this didn't let me customise the buttons as much as I wanted to. Therefore I used `createImg()` and then gave that image the functions of a button. The buttons where assigned a `.mouseClicked()`, that would tell them what to do when clicked. This was determined in the subsequent functions, which told each button what image to switch the background to. 

![](defaultButtons.png) *Default vs custom buttons*

![](myProject.png) *My project*

### How would you describe your first independent coding experience?
I would describe my first coding experience in this semester as interesting, fun, challenging and succesful. I encountered many problems and many, _many_ blank pages, but in the end it all worked out! For me it was very helpful to look at other people's code to see, how they made x thing work. I think it's important to look at other peoples code as a beginner in order to learn more, but I also knew that I didn't want to copy-paste as that doesn't give you the same sense of understanding as writing it yourself. There are things I wish I could've figured out how to do to make everything a little bit nicer (such as using gifs instead of images). One of our instructors did explain to me how I would be able to use gifs, but I didn't have more energy left to actually do that, so I let it be images since the idea I had still became a reality. I'm excited to become better, but for my first MiniX1 I'm happy that it serves a (to me) meaningful purpose.

### How is the coding process different from, or similar to, reading and writing text?
For me I think that the difference lies in the receiver. When writing code you're writing very specific commands, and if it's not entirely correct you'll encounter a problem. If you're writing a message to someone and use some slightly wrong words the receiver should still be able to understand what was intended with your message. If you're learning a new language however and therefore use completely wrong words at times the receiver would probably still be able to understand, what you were trying to say, because they're aware of your current skill level in the given language. Coding does not take beginners mistakes into consideration in the same way. You'll be punished for the smallest mistakes, which could prove very irritating and frustrating to beginners. 

### What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?
The text "Coding for Everyone and the Legacy of Mass Literacy" by Annette Vee informs you about the importance of computer literacy as it will be a condition for employment. That makes sense as it would prove beneficial to know how to create and modify software in a world where technology is all around us. To me programminig is important in the sense that I think more people would be interested in hiring me if I know how to read and write code, but I also see programming and coding as a creative outlet. It is similar to writing your own crochet pattern or making something out of clay because you in those two examples are free to decide, what you want to create - I get a similar experience with coding. 


**References**
- Soon Winnie & Cox, Geoff, "Getting started", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 27-48
- Annette Vee, "Coding for Everyone and the Legacy of Mass Literacy", Coding Literacty: How Computer Programming Is Changing Writing Cambridge, Mass.: MIT Press, 2017), 43-93. (on blackbord under the Literature folder)
