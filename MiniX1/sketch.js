// Declare variables (Shortcut/reference).
let eggBasic, eggGood, eggBad, badButton, basicButton, goodButton, returnButton, frontPage, frontBad, frontBasic, frontGood;

// Pre-render media (prevents lag before image appears after pressing buttons).
function preload() {
  eggBad = loadImage('yolk.png');
  eggBasic = loadImage('egger.png');
  eggGood = loadImage('chick.png');
  frontPage = loadImage('frontPageImage.png');
  frontBad = loadImage('badButton.png');
  frontBasic = loadImage('basicButton.png');
  frontGood = loadImage('goodButton.png');
  frontReturn = loadImage('returnButton.png');
}

// Individual button names, "createImg('insert-image')" for custom-made button, position, size/style, action when pressed(change BG).
function setup() {
  createCanvas(600, 600);
  background(frontPage);
  badButton = createImg('badButton.png');
  badButton.position(80, 500);
  badButton.style("width","100px");
  badButton.mouseClicked(changeBGBad);

  basicButton = createImg('basicButton.png');
  basicButton.position(250, 500);
  basicButton.style("width","100px");
  basicButton.mouseClicked(changeBGBasic);

  goodButton = createImg('goodButton.png');
  goodButton.position(420, 500);
  goodButton.style("width","100px");
  goodButton.mouseClicked(changeBGGood);

  returnButton = createImg('returnButton.png');
  returnButton.position(540, 13);
  returnButton.style("width","44px");
  returnButton.mouseClicked(changeBGFrontPage);
}

// Controls the functions assigned to each button.
function changeBGBad() {
  background(eggBad);
}

function changeBGBasic() {
  background(eggBasic);
}

function changeBGGood() {
  background(eggGood);
}

function changeBGFrontPage() {
  background(frontPage);
}
