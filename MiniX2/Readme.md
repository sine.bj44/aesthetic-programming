## MiniX2

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX2/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX2/sketch.js)

### Describe your program and what you have used and learnt
The idea was to create something that could show how emojis have the power to make it easier to lie about and to mask how you're truly feeling. It had to be something that would indicate, that if you were to take a closer look at the person using the happy emojis they might not actually be feeling that way. To visualise this I created a character with a very generic happy face, where the face changes if you hover over it with the magnifying glass attached to the mouse. The magnifying glass should be seen as sort of a metaphor for taking a closer look.

In this week's miniX I decided to use the `if-else` statement to hopefully learn how it works. I also decided that I wanted to learn what object-oriented programming is and how to use it. Therefore I created a `class` as well. Since I didn't use any objects in the previous miniX I tried to implement it into this week's miniX. I ended up using a `vertex` and an `ellipse` to create my magnifying glass, where I then also used `translate(mouseX,mouseY)` to attach it to the mouse - which is also something I haven't tried doing before. If you're interested in seeing how all these work then try looking at my code, since I tried to explain them to myself in it. [_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX2/sketch.js)

![](myProject.png) *What you see when you first open my project - and what happens to the face when you place the magnifying glass over it*

### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism and so on?
I decided to focus on how I could create an emoji that would show how emojis sometimes are used. My focus was on how they don't always represent the feeling a person is actually feeling and how some information that you would've usually picked up on (fx that someone's sad) might get lost because the emoji gives you misleading information. I got the idea when I was thinking about how some emojis have different meanings to different age groups, and then I started looking into how they also have different meanings in different contexts. My emoji might not be something you could use as an actual emoji, but it can be used to show how emojis in a way have a lot of power when two (or more) people are communicating. 

**Codes I got inspired by/looked at"
https://editor.p5js.org/kjhollen/sketches/dHOoxK_hD

**References**
- Soon Winnie & Cox, Geoff, "Variable Geometry", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 51-70
- Video/text: Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, "Modifying the Universal." Executing Practices, Eds. Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, London: Open Humanities Press, 2018, 35-51. OR Femke Snelting, Modifying the Universal, MedeaTV, 2016 
