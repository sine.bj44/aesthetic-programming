// Declare global variables.
  // If these were inside a function you could only call upon them in that specific function not outside of it (local variables).
let happy, realFace, wallpaper, faceChange;

// Preload the images to prevent lag.
function preload() {
  dotty = loadImage('yellowDot.png');
  realFace = loadImage('propellaSad_Face.png');
  wallpaper = loadImage('propellaText.png');
  }

function setup() {
  createCanvas(600, 700);
  // Tell it to use the class InteractionCirle to make "faceChange". Also, tell it where it should be placed on the canvas.
  faceChange = new InteractionCirle(196, 361, realFace);
}

// Place the background in draw() so it doesn't make the magnifying glass show up a billion times.
function draw() {
  background(wallpaper);
  // This makes the "faceChange" show up.
  faceChange.display();

// Create and assign the magnifying glass to the mouse.
  translate(mouseX,mouseY);
  // Creation of the handle of the magnifying glass.
  noStroke();
  fill(0)
  beginShape();
    vertex(50, 88, 200, 100);
    vertex(73, 70);
    vertex(160, 170);
    vertex(138, 190);
  endShape(CLOSE);

  // Creation of the glass circle.
  strokeWeight(10)
  stroke(5);
  fill(255, 126);
  ellipse(0, 0, 200, 200);
}

// In the class() section it is specified what it means to be an interactionCirle. Setup() gets told to show that class, which means it'll show everything this class consists of.
class InteractionCirle {
  // The class InteractionCirle is constructed of (X, Y, Img).
  constructor(X, Y, Img) {
    // Define what (X, Y, Img) is. Fx you attach the property of 'dotty' to this object ➜ this.dotty = Img.
    this.x = X;
    this.y = Y;
    this.dotty = Img;
  }

  // If over() is activated then the if statement tells it to show image(realFace) else show image(dotty).
  display() {
    if (this.over()) {
    image(realFace, 214, 386);
    }
    else {
    image(dotty, 270, 400);
    }
  }

  // Define what over() does ➔ if mouse is over the face then return to true (realFace image) else return false (dotty image).
  over() {
    if (mouseX > this.x && mouseX < this.x + this.dotty.width && mouseY > this.y && mouseY < this.y + this.dotty.height) {
      return true;
    } else {
      return false;
    }
    }
  }
