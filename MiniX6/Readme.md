## MiniX6

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX6/)

[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX6/sketch.js)

### What have you changed and why?
I chose to rework my [MiniX5](https://gitlab.com/sine.bj44/aesthetic-programming/-/tree/master/MiniX5). I initially wanted to make a whole new project that was more emergent, but after reading the assigned material I decided to just look critically at what I had already made. I decided to look away from the emergent aspect of it and see if there was anything else I could change. I realised that I might've unintentionally made a program that promotes a negative way of addressing people with scoliosis. If someone who has never heard of scoliosis saw my project they might unintentionally start assosiating scoliosis with the negative words because they're more dominant. Therefore I decided to change the program to first show my own critique of my program which I've done by making the negative words even more dominant. I also made a different one where I made the positive words more dominant in an attempt to fix the critique of my project. This change however ruined the whole meaning of the project: "Which words have the most impact?".

_My project with the different dominant words (It's a bit blurry because I had to resize the image)_
![](myProject.png)

This made me think that maybe I couldn't actually fix the critique of my project. Maybe sometimes we create things that are made for a good purpose, but it ends up being ruined by humans. My project does portray a good message: "Think about what you say to someone", but because of humans (even if it's not intentional) my project could potentionally turn into something bad. This realisation made me think of the invention of the like button on Facebook (I watched a movie about it once, but I can't remember the name for refence sorry). It wasn't invented to create the amount of stress and other negative effects it has on people. It was just implemented to give the users a way to let the poster know that they liked the post. So maybe some problems aren't as easily fixed because sometimes when you create a good thing you can't predict what effect it'll end up having. 

### How would you demonstrate aesthetic programming in your work?
I try to always think something conceptuel into my projects since Aesthetic Programming is not just about making a program that works - it's not even about making it look aesthetically pleasing. It's conveying a message and to use it as a tool to express something fx critical. I definitely feel like I have tried to use my program as a tool to express how words often have a big impact on people.

While thinking about this weeks assigned reading I also started thinking about how I use my programs to convey a message, but I don't really bring these messages _into my programs_. So far it has only gone one way - it doesn't come full circle. Therefore I've changed some of the names in my code. Before the images were named "bubble1, bubble2 ...", but I've changed them to "correct(1,2 3 ...)" or "incorrect(1, 2, 3 ...)". I did this to imitate how it may feel for the reciever of these words. If you hear mostly negative words then you often end up believing it's correct. This way I brought the message into the code. I felt as if this made you think about the meaning of my project by even just looking at my code. It might not convey the message just as well, but at least now the code isn't only a practical thing.

![](myCode.PNG) *The changes in my code*

### What does it mean by programming as a practice, or even as a method for design?
Programming as a method for design instead of a practice is a quite interesting thing as it lets you express your ideas in a relatively easy way. Instead of you only using programming as a tool to create something practical you can also use it as a way to design something. 

### What is the relation between programming and digital culture?
I'm not sure how to answer this question, but I definitely think that these two are closely related. I think if you have an understanding of how much power a programmer has you also become more wary of how you use technology. You get an understanding of how nothing is randomly placed and how what you're using online might've been created to make you do a certain thing or feel a certain emotion.

### Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical-aesthetics?
I do feel as if these two quotes fit together/link quite nicely, as they both explain how programming can be used to create things and produce critique upon different topics/things:

> _"Technological artifacts embody cultural values, and that technological development can be combined with cultural reflectivity to build provocative objects that encourage a re-evaluation of the role of technology in culture"_ - Critical Making and Interdisciplinary Learning by Matt ratto & Garnet Hertz, p. 19

> _"Aesthetic programming in this sense is considered as a practice to build things, and makeworlds, but also produce immanent critique drawing upon computer science, art, and cultural theory."_ - Aesthetic Programming (Praface) by Winnie Soon and Geoff Cox, p. 15

I do feel as if my work demonstrates the perspective of critical-aesthetics in the sense that I did create something that is critical, but I created it through a design. 

**References**
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28
