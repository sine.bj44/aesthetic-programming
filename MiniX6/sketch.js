let spacing = 137.8;
let spacing2 = 138.1;
let aX = -131;
let aY = -230;
let bX = -138;
let bY = 0;

function preload() {
    spine = loadImage('images/spine.png');

//negative impact - positive impact  - Beautiful - incorrect  ugly - correct information
    correct1 = loadImage('images/ABNORMAL2.png');
    incorrect1 = loadImage('images/BEAUTIFUL.png');
    correct2 = loadImage('images/DEFORMED2.png');
    correct3 = loadImage('images/HIDEOUS2.png');
    correct4 = loadImage('images/QUASIMODO2.png');
    incorrect2 = loadImage('images/RESOLUTE.png');
    incorrect3 = loadImage('images/STRONG.png');
    correct5 = loadImage('images/UGLY2.png');
    correct6 = loadImage('images/WRONG2.png');
    correct7 = loadImage('images/MONSTROUS2.png');

    skin1 = loadImage('images/skin1.png');
    skin2 = loadImage('images/skin2.png');
    skin3 = loadImage('images/skin3.png');
    skin4 = loadImage('images/skin4.png');
    skin5 = loadImage('images/skin5.png');
    skin6 = loadImage('images/skin6.png');
    skin7 = loadImage('images/skin7.png');
    skin8 = loadImage('images/skin8.png');

//øgenavne i stedet for bubbles
    nicknames = [correct1, incorrect1, correct2, correct3, correct4, incorrect2, incorrect3, correct5, correct6, correct7];
    colours = [skin1, skin2, skin3, skin4, skin5, skin6, skin7, skin8];
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(2);
}

function draw() {
  image(random(nicknames), aX, aY);
  aX += spacing;
    if (aX >= width) {
        aX = -135;
        aY += 230;
      }

  image(random(colours), bX, bY);
  bX += spacing2;
    if (bX >= width) {
        bX = -138;
        bY += 230;
      }

    for (var x = 0; x <= width; x = x + 276) {
        for (var y = 0; y <= width; y = y + 230) {
            image(spine, x, y);
        }
    }
}
