let spacing = 137.8;
let spacing2 = 138.1;
let aX = -131;
let aY = -230;
let bX = -138;
let bY = 0;

function preload() {
    spine = loadImage('images/spine.png');

    bubble1 = loadImage('images/ABNORMAL.png');
    bubble2 = loadImage('images/BEAUTIFUL.png');
    bubble3 = loadImage('images/DEFORMED.png');
    bubble4 = loadImage('images/HIDEOUS.png');
    bubble5 = loadImage('images/QUASIMODO.png');
    bubble6 = loadImage('images/RESOLUTE.png');
    bubble7 = loadImage('images/STRONG.png');
    bubble8 = loadImage('images/UGLY.png');
    bubble9 = loadImage('images/WRONG.png');
    bubble10 = loadImage('images/MONSTROUS.png');

    skin1 = loadImage('images/skin1.png');
    skin2 = loadImage('images/skin2.png');
    skin3 = loadImage('images/skin3.png');
    skin4 = loadImage('images/skin4.png');
    skin5 = loadImage('images/skin5.png');
    skin6 = loadImage('images/skin6.png');
    skin7 = loadImage('images/skin7.png');
    skin8 = loadImage('images/skin8.png');

    bubbles = [bubble1, bubble2, bubble3, bubble4, bubble5, bubble6, bubble7, bubble8, bubble9, bubble10];
    colours = [skin1, skin2, skin3, skin4, skin5, skin6, skin7, skin8];
}

function setup() {
    createCanvas(windowWidth, windowHeight);
    frameRate(2);
}

function draw() {
  image(random(bubbles), aX, aY);
  aX += spacing;
    if (aX >= width) {
        aX = -135;
        aY += 230;
      }

  image(random(colours), bX, bY);
  bX += spacing2;
    if (bX >= width) {
        bX = -138;
        bY += 230;
      }

    for (var x = 0; x <= width; x = x + 276) {
        for (var y = 0; y <= width; y = y + 230) {
            image(spine, x, y);
        }
    }
}
