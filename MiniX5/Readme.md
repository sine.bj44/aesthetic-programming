## MiniX5

[_Click to view my **project**_](https://sine.bj44.gitlab.io/aesthetic-programming/MiniX5/)


[_Click to view my **code**_](https://gitlab.com/sine.bj44/aesthetic-programming/-/blob/master/MiniX5/sketch.js)

### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?
To be completely fair I was really confused all week. I definitely want to return to this weeks task next week (as I've understood it we're going to improve a previous miniX of ours next week). I strongly think I did this weeks miniX completely wrong, BUT for this week I decided that the rules would be that whenever x hits the end go to the next line, and whenever y hits the end go to the next line. Both x and y would be displayed in random forms. I also added a for loop to sort of set up the outlines. 

My program turns into a full page of spines with scoliosis in different skintones. Different words will pop up in the bubbles assigned to each spine. The whole program is sort of to ask the question "What words make the most impact?". I let it have more negative words than positive words, to sort of show how negative words often stick with us for a much longer time than the positive words. I can't actually know if it'll show you mostly negative words since it's random, and it could also turn out racist if a skin colour never shows up. 

I got the words by asking people of a scoliosis community what some of the worst words they've been told are and I also asked what good words they've been told that they remember the most.

![](myProject.png) *My project*

### How does this MiniX help you to understand the idea of “auto-generator”
Through an incredible amount of trial and error I eventually understood more about what auto-generated art should be. I don't really feel like mine fits it, but I got really frustrated with myself in the end and I just knew that I wanted to try something way more random in a sense and with rules that fit this topic better next week.

**References**
- Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142
- Nick Montfort et al. “Randomness,” 10 PRINT CHR$(205.5+RND(1)); : GOTO10, https://10print.org/ (Cambridge, MA: MIT Press, 2012), 119-146
